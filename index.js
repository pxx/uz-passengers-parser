const fs = require('fs');
const os = require('os');
const bluebird = require('bluebird');
const fileParser = require('./fileParser');
const _ =require('lodash');

const wrapQuotes = (value) => `"${value}"`;

function writeResults(results) {
  const ws = fs.createWriteStream('./results.csv');

  const columns = [
    'Дата',
    'Всего пассажиров',
    'Из Москвы',
    'На Москву',
    ...Object.keys(results[0].passengersByDirectionType),
    ...Object.keys(results[0].passengersByCarType),
    ...Object.keys(results[0].passengersByDocumentType),
    ...['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
    'Топ международных',
  ];
  ws.write(`${columns.map(col => `"${col}"`).join(';')}${os.EOL}`);
  results.forEach(result => {
    const row = [
      wrapQuotes(result.date),
      result.passengers,
      result.fromMoscow,
      result.toMoscow,
      ...Object.values(result.passengersByDirectionType),
      ...Object.values(result.passengersByCarType),
      ...Object.values(result.passengersByDocumentType),
      ...result.passengersByDayOfWeek,
      wrapQuotes(Object.entries(result.topInternationals)
        .map(([,[dir, pass]]) => `${dir}: ${pass}`)
        .join(', ')
      ),
    ];
    ws.write(`${row.join(';')}${os.EOL}`);
  });
  ws.end();
}

async function main() {
  const time = process.hrtime();

  const fileNames = fs.readdirSync('./data/csv');
  const dates = fileNames
    .map(fileName => {
      try {
        const [, month, year] = fileName.match(/f35_(\d+)_(\d+)\.csv/);
        return [year, month];
      } catch(err) {
        return null;
      }
    })
    .filter(Boolean)
    .sort((a, b) => a.join('').localeCompare(b.join('')));

  const shortDates = [['2020', '01']];
  const results = await bluebird.mapSeries(dates, async ([year, month]) => {
    console.log(`Parsing ${year}-${month}...`);
    const data = await fileParser(`./data/csv/f35_${month}_${year}.csv`);
    return {
      ...data,
      year,
      month,
      date: `${year}-${month}`,
    };
  });

  // Lost trains. A lot of them
  for (let i = 0; i < results.length; i++) {
    if (i === results.length - 1) {
      results[i].lostTrains = [];
    } else {
      const thisTrains = Object.keys(results[i].passengersByTrainNumber);
      const diffs = [];
      for (let j = i + 1; j < results.length; j++) {
        const nextTrains = Object.keys(results[j].passengersByTrainNumber);
        diffs.push(_.difference(thisTrains, nextTrains));
      }

      results[i].lostTrains = _.intersection.apply(null, diffs);
    }
  }

  writeResults(results);
  console.log('Thats all');
  console.log(process.hrtime(time));
}

main();
