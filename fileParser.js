const fs = require('fs');
const readline = require('readline');
const stream  = require('stream');
const _ = require('lodash');

module.exports = (fileName) => {
  return new Promise((resolve, reject) => {
    const instream = fs.createReadStream(fileName);
    // const instream = fs.createReadStream('./data/test.csv');
    const outstream = new stream();
    const rl = readline.createInterface(instream, outstream);

    const results = {
      linesCount: 0,
      passengers: 0,
      fromMoscow: 0,
      toMoscow: 0,
      passengersByTrainNumber: {},
      passengersByDirectionType: {},
      passengersByDayOfWeek: Array(7).fill(0),
      passengersBySourceStation: {},
      passengersByTargetStation: {},
      passengersByCarType: {},
      passengersByDocumentType: {},
      internationals: {},
    };

    let headerSkipped = false;

    rl.on('line', (line) => {
      if (!headerSkipped) {
        headerSkipped = true;
        return;
      }

      results.linesCount++;
      const preparedLine = line
        .split(';')
        .map(text => text.replace(/"/g, '').trim())
        // Sometimes first column is empty column. E.g. f35_(03,04,05)_2019
        .filter(value => value  !== '');

      // Sometimes 1st column is index. E.g. f35_12_2019
      if (/^\d+$/.test(preparedLine[0])) {
        preparedLine.shift();
      }

      let [
        trainNumber,
        directionType,
        date,
        sourceStation,
        targetStation,
        carType,
        documentType,
        passengers
      ] = preparedLine;
      passengers = parseInt(passengers, 10);

      if (Number.isNaN(passengers)) {
        return;
      }


      results.passengers += passengers;

      if (results.passengersByTrainNumber[trainNumber]) {
        results.passengersByTrainNumber[trainNumber] += passengers;
      } else {
        results.passengersByTrainNumber[trainNumber] = passengers;
      }

      if (results.passengersByDirectionType[directionType]) {
        results.passengersByDirectionType[directionType] += passengers;
      } else {
        results.passengersByDirectionType[directionType] = passengers;
      }

      if (directionType === 'Міжнародне') {
        const dir = `${sourceStation} - ${targetStation}`;
        if (results.internationals[dir]) {
          results.internationals[dir] += passengers;
        } else {
          results.internationals[dir] = passengers;
        }
      }

      results.passengersByDayOfWeek[(new Date(date).getDay() + 6) % 7] += passengers;

      if (results.passengersBySourceStation[sourceStation]) {
        results.passengersBySourceStation[sourceStation] += passengers;
      } else {
        results.passengersBySourceStation[sourceStation] = passengers;
      }

      if (results.passengersByTargetStation[targetStation]) {
        results.passengersByTargetStation[targetStation] += passengers;
      } else {
        results.passengersByTargetStation[targetStation] = passengers;
      }

      if (results.passengersByCarType[carType]) {
        results.passengersByCarType[carType] += passengers;
      } else {
        results.passengersByCarType[carType] = passengers;
      }

      if (results.passengersByDocumentType[documentType]) {
        results.passengersByDocumentType[documentType] += passengers;
      } else {
        results.passengersByDocumentType[documentType] = passengers;
      }

      results.fromMoscow += sourceStation.toLowerCase().includes('москва') ? passengers : 0;
      results.toMoscow += targetStation.toLowerCase().includes('москва') ? passengers : 0;
    });

    rl.on('close', () => {
      instream.close();

      results.trainNumbers = Object.keys(results.passengersByTrainNumber).length;
      results.topSourceStations = _(results.passengersBySourceStation)
        .toPairs()
        .sort((a, b) => b[1] - a[1])
        .take(10)
        .value();
      results.topTargetStations = _(results.passengersByTargetStation)
        .toPairs()
        .sort((a, b) => b[1] - a[1])
        .take(10)
        .value();
      results.topInternationals = _(results.internationals)
        .toPairs()
        .sort((a, b) => b[1] - a[1])
        .take(10)
        .value();

      resolve(results);
    });
  });
};
